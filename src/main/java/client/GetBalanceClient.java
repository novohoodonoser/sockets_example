package client;


import com.squareup.okhttp.*;
import common.RequestsAndResponse;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class GetBalanceClient {

    public static void main(String[] args) throws IOException {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/xml");
        RequestBody body = RequestBody.create(mediaType, RequestsAndResponse.GET_BALANCE);
        Request request = new Request.Builder()
                .url("http://localhost:80/")
                .post(body)
                .addHeader("content-type", "application/xml")
                .addHeader("cache-control", "no-cache")
                .addHeader("postman-token", "29677cc7-2a6f-6099-c5dc-511bc8908e21")
                .build();
        client.setReadTimeout(4, TimeUnit.MINUTES);
        Response response = client.newCall(request).execute();
        System.out.println(response);
        System.out.println(response.body().string());
    }
}
