package services;


import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Map;

public interface ResponseService {

    byte[] getResponse(int xmlCode, Map<String,String> extraParamsWithValue) throws ParserConfigurationException, IOException, SAXException;
}
