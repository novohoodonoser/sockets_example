package services;


import common.RequestsAndResponse;
import common.XMLReceivedRequest;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import static common.RequestsAndResponse.DEFAULT_RESPONSE;

public class ResponseServiceImpl implements ResponseService {

    @Override
    public byte[] getResponse(int xmlCode, Map<String, String> extraParamsWithValue) {
        String data = "";
        String header ="";
        String defaultHeader = "HTTP/1.1 200 OK\r\n"
                +"Content-length: " +DEFAULT_RESPONSE.length() + "\r\n"
                +"Content-type: " + "application/xml" + "; charset=UTF-8\r\n\r\n";
        try{
        StringReader stringReader = new StringReader(RequestsAndResponse.RESPONSE);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(stringReader));
        XMLReceivedRequest request = new XMLReceivedRequest();
        document.getDocumentElement().normalize();
        NodeList nList = document.getElementsByTagName("response");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                eElement.getElementsByTagName("result-code").item(0).setTextContent(String.valueOf(xmlCode));
                if (extraParamsWithValue != null && !extraParamsWithValue.isEmpty()) {
                    for (String extraName : extraParamsWithValue.keySet()) {
                        Element extra = document.createElement("extra");
                        extra.setAttribute("name", extraName);
                        extra.setTextContent(extraParamsWithValue.get(extraName));
                        eElement.insertBefore(extra, eElement.getNextSibling());
                    }
                }
            }
        }
        TransformerFactory tf = TransformerFactory.newInstance();

            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(document), new StreamResult(writer));
            data = writer.getBuffer().toString();
            header = "HTTP/1.1 200 OK\r\n"
                    +"Content-length: " +data.length() + "\r\n"
                    +"Content-type: " + "application/xml" + "; charset=UTF-8\r\n\r\n";
        } catch (TransformerException | ParserConfigurationException | IOException | SAXException e) {
            return (defaultHeader + DEFAULT_RESPONSE).getBytes(Charset.forName("UTF-8"));
        }
        return (header +data).getBytes(Charset.forName("UTF-8"));
    }
}
