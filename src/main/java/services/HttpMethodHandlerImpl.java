package services;


import common.RequestType;
import common.XMLReceivedRequest;
import model.services.ModelOperationsService;
import model.services.ModelOperationsServiceImpl;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import xml.InputXMLParser;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class HttpMethodHandlerImpl implements HttpMethodHandler {

    private final ModelOperationsService modelOperationsService;

    public HttpMethodHandlerImpl(ModelOperationsService modelOperationsService) {
        this.modelOperationsService = modelOperationsService;
    }

    @Override
    public byte[] doPost(BufferedReader inputReader, ModelOperationsServiceImpl modelOperationsService) {
        int responseCode = 2;
        try {
            XMLReceivedRequest request = getRequestInfoFromInputStream(inputReader);
            if (RequestType.CREATE_NEW_CLIENT.equals(request.getRequestType())) {
                responseCode = modelOperationsService.createNewClient(request);
                return new ResponseServiceImpl().getResponse(responseCode,null);
            }
        } catch (IOException | SAXException | ParserConfigurationException ex) {
            return new ResponseServiceImpl().getResponse(2,null);
        }
        return new ResponseServiceImpl().getResponse(responseCode,null);
    }

    @Override
    public byte[] doGet(BufferedReader inputReader, ModelOperationsServiceImpl modelOperationsService) {
        int responseCode = 2;
        Map<String,String> extraParamsWithValue = new HashMap<>();
        try {
            XMLReceivedRequest request = getRequestInfoFromInputStream(inputReader);
            if (RequestType.GET_BALANCE.equals(request.getRequestType())) {
                BigDecimal balance = modelOperationsService.getBalance(request);
                responseCode=0;
                extraParamsWithValue.put("balance",balance.toString());
            }
        } catch (IOException | SAXException | ParserConfigurationException ex) {
            return new ResponseServiceImpl().getResponse(2,null);
        }
        return new ResponseServiceImpl().getResponse(responseCode,null);
    }


    private XMLReceivedRequest getRequestInfoFromInputStream(BufferedReader inputReader) throws IOException, ParserConfigurationException, SAXException {
        StringBuilder stringBuilder = new StringBuilder();
        getContentLength(inputReader, stringBuilder);
        return parseRequestFromReaderToRequestType(stringBuilder);
    }

    private XMLReceivedRequest parseRequestFromReaderToRequestType(StringBuilder request) throws ParserConfigurationException, IOException, SAXException {
        StringReader stringReader = new StringReader(request.toString());
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(stringReader));
        return InputXMLParser.getRequest(document);
    }

    private int getContentLength(BufferedReader inputReader, StringBuilder request) throws IOException {
        String inline;
        int contentLength = -1;
        int c;
        while (!(inline = inputReader.readLine()).equals("")) {
            if (inline.contains("Content-Length: ")) {
                contentLength = Integer.parseInt(inline.substring("Content-Length: ".length()));
            }
        }
        while (contentLength != 0 && (c = inputReader.read()) != -1) {
            request.append((char) c);
            contentLength--;
        }
        return contentLength;//TODO: Exception
    }

}
