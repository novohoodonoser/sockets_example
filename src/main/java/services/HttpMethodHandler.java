package services;


import model.services.ModelOperationsServiceImpl;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.IOException;

public interface HttpMethodHandler {

    byte[] doPost(BufferedReader inputReader, ModelOperationsServiceImpl modelOperationsService);

    byte[] doGet(BufferedReader inputReader, ModelOperationsServiceImpl modelOperationsService);

}
