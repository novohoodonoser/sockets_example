package common;

public class Constants {

    //Commands
    public static final String CREATE_NEW_USER ="CREATE-AGT";
    public static final String GET_BALANCE ="GET-BALANCE";

    //Fields
    public static final String LOGIN_STR = "login";
    public static final String PASSWORD_STR = "password";

}
