package common;

import java.util.HashMap;
import java.util.Map;

public class XMLReceivedRequest {

    RequestType requestType;

    Map<String,String> params = new HashMap<>();

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void addParams(String name, String value) {
        this.params.put(name,value);
    }

    @Override
    public String toString() {
        return "common.XMLReceivedRequest{" +
                "requestType=" + requestType +
                ", params=" + params +
                '}';
    }
}
