package common;


public class SocketRequestException extends Exception {

    public SocketRequestException(String message) {
        super(message);
    }
}
