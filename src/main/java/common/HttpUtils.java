package common;


public class HttpUtils {

    public static RequestMethod getHttpMethod(String inputString) {
        if (RequestMethod.POST.toString().equals(inputString.substring(0, 4))) {
            return RequestMethod.POST;
        } else if (RequestMethod.GET.toString().equals(inputString.substring(0, 4))) {
            return RequestMethod.GET;
        }
        return null; //TODO: Exception
    }
}
