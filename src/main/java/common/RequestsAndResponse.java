package common;


public class RequestsAndResponse {

    //Requests
    public static final String NEW_CLIENT = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n <request>\r\n    <request-type>CREATE-AGT</request-type>\r\n    <extra name=\"login\">123456</extra>\r\n    <extra name=\"password\">pwd</extra>\r\n</request>";
    public static final String GET_BALANCE = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n <request>\r\n   <request-type>GET-BALANCE</request-type>\r\n   <extra name=\"login\">123456</extra>\r\n    <extra name=\"password\">pwd</extra>\r\n</request>";
    //Response
    public static final String RESPONSE = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n  <response>\r\n  <result-code>0</result-code>\r\n </response>";
    public static final String DEFAULT_RESPONSE = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n  <response>\r\n  <result-code>2</result-code>\r\n </response>";

}
