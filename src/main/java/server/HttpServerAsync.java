package server;

import common.HttpUtils;
import common.RequestMethod;
import model.services.ModelOperationsServiceImpl;
import services.HttpMethodHandler;
import services.HttpMethodHandlerImpl;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.concurrent.Future;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.ExecutionException;
import java.net.InetSocketAddress;

public class HttpServerAsync {
    private final HttpMethodHandler httpMethodHandler;

    public HttpServerAsync(HttpMethodHandler httpMethodHandler) {
        this.httpMethodHandler = httpMethodHandler;
    }

    public static void main(String[] args)
            throws Exception {

        new HttpServerAsync(new HttpMethodHandlerImpl(new ModelOperationsServiceImpl())).go();
    }

    private void go()
            throws IOException, InterruptedException, ExecutionException {

        AsynchronousServerSocketChannel serverChannel = AsynchronousServerSocketChannel.open();
        InetSocketAddress hostAddress = new InetSocketAddress("localhost", 3883);
        serverChannel.bind(hostAddress);
        Future<AsynchronousSocketChannel> acceptResult = serverChannel.accept();
        AsynchronousSocketChannel clientChannel = acceptResult.get();
        if ((clientChannel != null) && (clientChannel.isOpen())) {
            boolean running = true;
            while (true && running) {
                ByteBuffer buffer = ByteBuffer.allocate(32);
                Future<Integer> result = clientChannel.read(buffer);

                while (!result.isDone() && running) {
                }

                buffer.flip();
                BufferedReader inputReader = new BufferedReader(new InputStreamReader((new ByteArrayInputStream(buffer.array()))));
                RequestMethod httpMethod = HttpUtils.getHttpMethod(inputReader.readLine());
                if (RequestMethod.POST.equals(httpMethod)) {
                    clientChannel.write(ByteBuffer.wrap(httpMethodHandler.doPost(inputReader, new ModelOperationsServiceImpl())));
                    running = false;
                } else if (RequestMethod.GET.equals(httpMethod)) {
                    clientChannel.write(ByteBuffer.wrap(httpMethodHandler.doGet(inputReader, new ModelOperationsServiceImpl())));
                    running = false;
                }

                buffer.clear();
            }
            clientChannel.close();
        }
        serverChannel.close();
    }
}

