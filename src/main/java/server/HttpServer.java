package server;


import common.HttpUtils;
import common.RequestMethod;
import model.services.ModelOperationsServiceImpl;
import services.HttpMethodHandler;
import services.HttpMethodHandlerImpl;
import services.ResponseServiceImpl;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;


public class HttpServer {

    private static final Logger logger = Logger.getLogger("HttpServer");

    private final int port;
    private static final int NUM_THREADS = 50;



    public HttpServer(  int port) {
        this.port = port;

    }

    public void start() {
        ExecutorService pool = Executors.newFixedThreadPool(NUM_THREADS);
        try (ServerSocket server = new ServerSocket(this.port)) {
            while (true) {
                try {
                    Socket connection = server.accept();
                    pool.submit(new HTTPHandler(connection,new HttpMethodHandlerImpl(new ModelOperationsServiceImpl())));
                } catch (IOException e) {
                    logger.log(Level.WARNING, "Exception accepting connection", e);
                } catch (RuntimeException e) {
                    logger.log(Level.SEVERE, "Unexpected error", e);
                }
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not start server", e);
        }
    }

    private class HTTPHandler implements Callable<Void> {
        private final Socket connection;
        private final HttpMethodHandler httpMethodHandler;

        public HTTPHandler(Socket connection, HttpMethodHandler httpMethodHandler) {
            this.connection = connection;
            this.httpMethodHandler =httpMethodHandler;
        }

        @Override
        public Void call() throws IOException {
            OutputStream out = new BufferedOutputStream((connection.getOutputStream()));
            try {
                BufferedReader inputReader = new BufferedReader(new InputStreamReader((connection.getInputStream())));
                RequestMethod httpMethod = HttpUtils.getHttpMethod(inputReader.readLine());
                if (RequestMethod.POST.equals(httpMethod)){
                    out.write(httpMethodHandler.doPost(inputReader, new ModelOperationsServiceImpl()));
                }else if (RequestMethod.GET.equals(httpMethod)){
                    out.write(httpMethodHandler.doGet(inputReader, new ModelOperationsServiceImpl()));
                }
                out.flush();
            } catch (IOException e) {
                out.write( new ResponseServiceImpl().getResponse(2,null));
            } finally {
                connection.close();
            }
            return null;
        }
    }




    public static void main(String[] args) {
        int port = 80;
        HttpServer server = new HttpServer( port);
        server.start();
    }
}
