package xml;

import common.Constants;
import common.XMLReceivedRequest;
import common.RequestType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;


public class InputXMLParser {

    public static XMLReceivedRequest getRequest(Document doc) {
        XMLReceivedRequest request = new XMLReceivedRequest();
        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("request");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                switch (eElement.getElementsByTagName("request-type").item(0).getTextContent()) {
                    case Constants.CREATE_NEW_USER:
                        request.setRequestType(RequestType.CREATE_NEW_CLIENT);
                        break;
                    case Constants.GET_BALANCE:
                        request.setRequestType(RequestType.GET_BALANCE);
                        break;
                }
                for (int extraParams = 0; extraParams < eElement.getElementsByTagName("extra").getLength(); extraParams++) {
                    Node parameter = eElement.getElementsByTagName("extra").item(extraParams);
                    String name = parameter.getAttributes().item(0).getNodeValue();
                    String value = parameter.getTextContent();
                    request.addParams(name, value);
                }
            }
        }
        return request;
    }
}
