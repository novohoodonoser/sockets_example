package model;

import java.io.Serializable;
import java.math.BigDecimal;

public class User implements Serializable {

    private Integer userId = null;
    private String name;
    private String password;
    private BigDecimal accountBalance;

    public User (){};

    public User(String name, String password, BigDecimal accountBalance) {
        this.name = name;
        this.password = password;
        this.accountBalance = accountBalance;
    }

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }
}
