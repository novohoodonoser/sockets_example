package model.dao;

import model.User;

import java.math.BigDecimal;

public interface UserDao {

    void create(User user);

    BigDecimal getBalance(User user);

}