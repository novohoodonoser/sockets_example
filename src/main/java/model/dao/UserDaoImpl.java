package model.dao;

import model.User;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by MI on 25-Sep-17.
 */
public class UserDaoImpl implements UserDao{
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;


    private Connection getConnection() throws SQLException {
        Connection conn;
        conn = ConnectionFactory.getInstance().getConnection();
        return conn;
    }

    public void create(User user) {
        try {
            String checkBeforeInsertString = "insert into users (name, password)\n" +
                    "  select\n" +
                    "    case when is_exist.ammount = 0\n" +
                    "      then cast(? as varchar (15))\n" +
                    "    else null end as name,\n" +
                    "    case when is_exist.ammount = 0\n" +
                    "      then cast(? as varchar (15))\n" +
                    "    else null end as password\n" +
                    "  from\n" +
                    "    (select count(1) ammount\n" +
                    "     from users tmp\n" +
                    "     where tmp.name = ?\n" +
                    "           and tmp.password = ?)\n" +
                    "      as is_exist";
            connection = getConnection();
            preparedStatement= connection.prepareStatement(checkBeforeInsertString);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getName());
            preparedStatement.setString(4, user.getPassword());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null)
                    preparedStatement.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public BigDecimal getBalance(User user) {
        BigDecimal balance = null;
        try {
            String queryString = "SELECT balance\n" +
                    "FROM account_balance ab, users u\n" +
                    "WHERE ab.user_id = u.user_id AND u.user_id = ? AND u.password = ?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getPassword());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                balance = resultSet.getBigDecimal("balance");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (preparedStatement != null)
                    preparedStatement.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return balance;
    }
}
