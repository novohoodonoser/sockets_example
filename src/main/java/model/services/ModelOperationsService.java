package model.services;


import common.XMLReceivedRequest;

import java.math.BigDecimal;

public interface ModelOperationsService {

    int createNewClient(XMLReceivedRequest request);
    BigDecimal getBalance(XMLReceivedRequest request);
}
