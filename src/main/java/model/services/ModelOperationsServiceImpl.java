package model.services;

import common.Constants;
import common.XMLReceivedRequest;
import model.User;
import model.dao.UserDao;
import model.dao.UserDaoImpl;

import java.math.BigDecimal;


public class ModelOperationsServiceImpl implements ModelOperationsService {

    @Override
    public int createNewClient(XMLReceivedRequest request) {
        UserDao userDao = new UserDaoImpl();
        User user = new User(request.getParams().get(Constants.LOGIN_STR), request.getParams().get(Constants.PASSWORD_STR));
        userDao.create(user);
        return 0;
    }

    @Override
    public BigDecimal getBalance(XMLReceivedRequest request) {
        UserDao userDao = new UserDaoImpl();
        User user = new User(request.getParams().get(Constants.LOGIN_STR), request.getParams().get(Constants.PASSWORD_STR));
        return userDao.getBalance(user);
    }
}
